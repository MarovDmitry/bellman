/bin - executable files
/lib - library files
/log - log files
/script - files used for convenience

# This library realizes universal Bellman's method for dynamic programming problems.
# This library lets programmer to solve formal dynamic programming problem by extending from library classes.
# Different dynamic programming problems may be solved the same way.
