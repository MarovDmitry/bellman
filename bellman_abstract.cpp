#include "BellmansMethod.hpp"


State::~State(){}
Control::~Control(){}
StateFunction::~StateFunction(){};
StateFunctionCollection::~StateFunctionCollection()
{
    for(auto *it : sfc_) delete it;
}
Function::~Function(){}
FunctionCollection::~FunctionCollection()
{
    for(auto *it : fc_) delete it;
}
OptimalFunction::~OptimalFunction(){}
OptimalControlFunction::~OptimalControlFunction(){}
OptimalControlFunctionCollection::OptimalControlFunctionCollection(){};

void OptimalControlFunctionCollection::append(OptimalControlFunction* ocf)
{
    ocfc_.push_back(ocf);
}

OptimalControlFunction* OptimalControlFunctionCollection::at(int num)
{
    return ocfc_.at(num);
}

OptimalControlFunctionCollection::~OptimalControlFunctionCollection()
{
    for(auto *it : ocfc_) delete it;
}

Set::~Set(){}

SetCollection::~SetCollection()
{
    for(auto *it : sc_) delete it;
}

Solver::Solver(int n,IOCContainer* c):n_(n),sfc_(c->getStateFunctionCollection()),fc_(c->getFunctionCollection()),sc_(c->getSetCollection()){}

std::list<FunctionReturnVector*> Solver::solve()
{
    std::list<FunctionReturnVector*> res;

    FunctionReturnVector *v;//result of current step

    Function *acc;//accumulative R function
    Function *RfromZ;//R'(x_k)=Z_{k+1}(s_k(x_k))
    OptimalFunction *z;//accumulated optimal function Z_k(s_{k+1})
    OptimalControlFunctionCollection* Ocfc;//set of optimal controls x_1, x_2 ...

    acc=(*fc_)(n_);//the last step function is just R_k(x_k)
    Ocfc=acc->optimize((*sc_)(n_));//all the optimal solutions
    z=acc->toOptimalFunction(Ocfc->ocfc_.at(0));//optimal Z function from one of optimal solutions (at least one)

    v=new FunctionReturnVector();
    v->s_=(*sc_)(n_);
    v->ocfc_=Ocfc;//all the optimal controls
    v->f_=(*fc_)(n_);//R function that was in use on this step
    v->sf_=(*sfc_)(n_);//S function that was in use on this step
    v->z_=z;//result function, calculated on this step
    res.push_back(v);

    for(auto I=n_-1;I>=1;I--)
    {
        RfromZ=z->toFunction((*sfc_)(I));
        acc=(*fc_)(I)->unite(RfromZ);
        Ocfc=acc->optimize((*sc_)(I));
        z=acc->toOptimalFunction(Ocfc->ocfc_.at(0));

        v=new FunctionReturnVector();
        v->s_=(*sc_)(I);
        v->ocfc_=Ocfc;
        v->f_=(*fc_)(I);
        v->sf_=(*sfc_)(I);
        v->z_=z;
        res.push_back(v);
    }

    return res;
}

std::list<ValueReturnVector*> Solver::calculate(State *s0,std::list<FunctionReturnVector*> V)
{
    std::list<ValueReturnVector*> res;
    State *buf=s0;
    int i=V.size();
    ValueReturnVector *v;
    for(auto it =V.rbegin(),e=V.rend();it!=e;it++)
    {
        v=new ValueReturnVector();


        for(auto *solution : (*it)->ocfc_->ocfc_)
        {
            v->cv_->push_back((*solution)(buf));
        }
        v->z_=(*((*it)->z_))(buf);
        buf=(*((*it)->sf_))(v->cv_->at(0));
        v->s_=buf;

        v->f_=(*((*it)->f_))(v->cv_->at(0));

        res.push_back(v);
        i--;
    }

    return res;
}

Solver::~Solver(){}

FunctionReturnVector::FunctionReturnVector(){}
FunctionReturnVector::~FunctionReturnVector()
{
    delete z_;
    delete ocfc_;
}
ValueReturnVector::ValueReturnVector()
{
    cv_=new std::vector<Control*>;
};
ValueReturnVector::~ValueReturnVector()
{
    delete s_;
    for(auto it : *cv_) delete it;
    delete cv_;
}
IOCContainer::~IOCContainer()
{
    delete sfc_;
    delete fc_;
    delete sc_;
}
