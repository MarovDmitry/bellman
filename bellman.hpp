#ifndef BELLMANSMETHOD_HPP_INCLUDED
#define BELLMANSMETHOD_HPP_INCLUDED

#include <vector>
#include <list>
#include <string>

class State;//class, that determines state   S_k
class Control;//class, that determines control   C_k

//{
class StateFunction;//class, that determines functional dependency of next state form previous control   S_k(x_{k-1})
class StateFunctionCollection;//class, that determines StateFunction on each step
//}   (because in some cases Statefunction can be the same and in some cases not)

class OptimalFunction;//class, that realizes target function, that accumulates result step by step   Z_k(S_{k-1})

//{
class OptimalControlFunction;//class, that determines functional dependency of optimal control from previous state    x_k(s_{k-1})
class OptimalControlFunctionCollection;//stores control optimal functions (can be not the only one)
//}

class Function;// class, that determines target function of current step  f_k(x_k(s_{k-1}))
class FunctionCollection;//class, that determines Function on each step
class Set;//class-container to store optimization information

class Solver;
class FunctionReturnVector;
class ValueReturnVector;

class State
{
    public:
        virtual std::string toString()=0;
        virtual ~State();
};

class Control
{
    public:
        virtual std::string toString()=0;
        virtual ~Control();
};

class StateFunction
{
    public:
        virtual State* operator()(Control *c)=0;
        virtual std::string toString(int k)=0;
        virtual ~StateFunction();
};

class StateFunctionCollection
{
    protected:
        std::vector<StateFunction*> sfc_;

    public:
        virtual StateFunction* operator()(int k)=0;// k - number of step (in reverse order)
        virtual ~StateFunctionCollection();
};

class Function
{
    public:
        virtual double operator()(Control* c)=0;
        virtual OptimalFunction* toOptimalFunction(OptimalControlFunction *x)=0;// Z_k=R(x_opt)=R_k(x_opt)+R'(x_opt)
        virtual OptimalControlFunctionCollection* optimize(Set* s)=0;// x1_opt, x2_opt ...
        virtual Function* unite(Function* f)=0;// because R(x_opt)=R_k(x_opt)+R'(x_opt)    but '+' in not necessary
        virtual std::string toString(int k)=0;
        virtual ~Function();
};

class FunctionCollection
{
    public:
        std::vector<Function*> fc_;
    public:
        virtual Function* operator()(int k)=0;// k is step
        virtual ~FunctionCollection();
};

class OptimalFunction
{
    public:
        virtual Function* toFunction(StateFunction *s)=0;// R'(x_k)=Z_{k+1}(s_k(x_k))
        virtual double operator()(State *s)=0;
        virtual std::string toString(int k)=0;
        virtual ~OptimalFunction();
};

class OptimalControlFunction
{
    public:
        virtual Control* operator()(State *s)=0;
        virtual std::string toString(int k)=0;
        virtual ~OptimalControlFunction();
};

//this class is in use because multiple optimal controls may exist
class OptimalControlFunctionCollection
{
    public:
        std::vector<OptimalControlFunction*> ocfc_;

    public:
        OptimalControlFunctionCollection();
        void append(OptimalControlFunction* ocf);
        OptimalControlFunction* at(int num);
        virtual ~OptimalControlFunctionCollection();
};

//this is just a set for storing values for optimization function
class Set
{
    public:
        virtual std::string toString(int k)=0;
        virtual ~Set();
};
//each step has (may be) different optimization set
//for example x_{k1}>=0;x_{k2}>=0;x_{k1}+x_{k2}<=s_{k-1}       in sectors fund distribution problem
class SetCollection
{
    public:
        std::vector<Set*> sc_;

    public:
        virtual Set* operator()(int k)=0;
        virtual ~SetCollection();
};

//class that returns propper collection classes for solver
class IOCContainer
{
    protected:
        StateFunctionCollection *sfc_;
        FunctionCollection *fc_;
        SetCollection *sc_;

    public:
        virtual StateFunctionCollection* getStateFunctionCollection()=0;//dependency state from previous control
        virtual FunctionCollection* getFunctionCollection()=0;//dependency of target function from control
        virtual SetCollection* getSetCollection()=0;//optimization set on each step
        virtual ~IOCContainer();
};

class Solver
{
    private:
        int n_;//number of steps
        StateFunctionCollection *sfc_;
        FunctionCollection *fc_;
        SetCollection *sc_;

    public:
        Solver(int n,IOCContainer* c);
        std::list<FunctionReturnVector*> solve();
        std::list<ValueReturnVector*> calculate(State *s0,std::list<FunctionReturnVector*> v);

        virtual ~Solver();
};

class FunctionReturnVector
{
    public:
        StateFunction* sf_;
        Function* f_;
        OptimalFunction* z_;
        OptimalControlFunctionCollection* ocfc_;
        Set *s_;

    public:
        FunctionReturnVector();
        virtual ~FunctionReturnVector();
};

class ValueReturnVector
{
    public:
        State *s_;
        std::vector<Control*> *cv_;
        double z_;
        double f_;

    public:
        ValueReturnVector();
        virtual ~ValueReturnVector();
};

#endif // BELLMANSMETHOD_HPP_INCLUDED
